#!/usr/bin/make -f

# Copyright 2007-2008 Steffen Moeller <moeller@debian.org>
#
#   This package is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This package is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this package; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
#
# On Debian systems, the complete text of the GNU General
# Public License can be found in `/usr/share/common-licenses/GPL'.


all: getData.1

getData.1: getData
	pod2man getData > getData.1

getData.txt: getData
	pod2text getData > getData.txt

commit: commit_flag

commit_flag: getData.1 getData.txt
	svn commit
	touch $@

clean:
	rm --force getData.1 getData.txt

VERSION=$(shell cat version)
dist: clean
	cd .. && tar --exclude .svn --exclude debian --exclude .pc -czvf getdata_$(VERSION).orig.tar.gz getData

.PHONY: commit clean all
