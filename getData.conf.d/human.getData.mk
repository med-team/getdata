ORGANISM   = Homo_sapiens
ORGANISM_L = homo_sapiens
BUILD      = GRCh37.56
NICKNAME   = hg19

include /etc/getData.conf.d/Ensembl_genome.mk
