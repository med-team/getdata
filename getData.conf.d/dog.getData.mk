SHARED_WGET_OPTIONS=$(shell getData --getWgetOptions)

get:
	wget $(SHARED_WGET_OPTIONS) ftp://ftp.ensembl.org/pub/current_fasta/canis_familiaris/dna/Canis_familiaris.BROADD2.50.dna.chromosome.*.fa.gz

unpack:
	for file in *chromosome.*.fa.gz ; do zcat $$file > `basename $$file .gz` ; done

blast:
	cat *fa | formatdb -i /dev/stdin -t CanFam2.0 -n canfam2 -p F

deb:
	equivs-build /etc/getData.conf.d/dog.getData.control
