SHARED_WGET_OPTIONS=$(shell getData --getWgetOptions)

# $SPECIES is provided to make in the call from /etc/getData.conf.d/RefSeq.getData.

get:
	wget $(SHARED_WGET_OPTIONS) ftp://ftp.ncbi.nih.gov/refseq/$(SPECIES)/mRNA_Prot/*ff.gz

unpack:
	for file in *ff.gz ; do zcat $$file > `basename $$file .gz` ; done

blast:

emboss:
