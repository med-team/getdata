SHARED_WGET_OPTIONS=$(shell getData --getWgetOptions)

MIRROR = ftp://ftp.ensembl.org/pub/current_fasta

get:
	wget $(SHARED_WGET_OPTIONS) $(MIRROR)/$(ORGANISM_L)/dna/$(ORGANISM).$(BUILD).dna.chromosome.*.fa.gz

unpack:
	for file in *chromosome.*.fa.gz ; do zcat $$file > `basename $$file .gz` ; done
