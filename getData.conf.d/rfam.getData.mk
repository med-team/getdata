SHARED_WGET_OPTIONS=$(shell getData --getWgetOptions)

get:
	wget $SHARED_WGET_OPTIONS ftp://ftp.sanger.ac.uk/pub/databases/Rfam/9.1/*

unpack:
	for file in *.gz ; do zcat $$file > `basename $$file .gz` ; done
